public class Student {
    private String name;
    private int matrikelnummer;

    public Student(String name, int matrikelnummer) {
        setName(name);
        setMatrikelnummer(matrikelnummer);
    }

    private static boolean checkName(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        } else return Character.isUpperCase(name.charAt(0));
    }

    private static boolean checkMatrikelnummer(int matrikelnummer) {
        return (matrikelnummer >= 1_000_000 && matrikelnummer <= 9_999_999);
    }

    public void setName(String name) {
        if (checkName(name)){
            this.name = name;
        } else {
            throw new IllegalArgumentException("Name must start with uppercase character");
        }
    }

    public void setMatrikelnummer(int matrikelnummer) {
        if (checkMatrikelnummer(matrikelnummer)) {
            this.matrikelnummer = matrikelnummer;
        } else {
            throw new IllegalArgumentException("Matrikelnummer must be a 7 digit int");
        }
    }

    public String getName() {
        return name;
    }

    public int getMatrikelnummer() {
        return matrikelnummer;
    }

    @Override
    public String toString() {
        return "(" + getMatrikelnummer() + ", " + getName() + ")";
    }
}
