import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

public class aufgabe22 {
    public static void main(String[] args) {
        a();
        b();
        c();
    }

    public static void a() {
        Random rand = new Random();
        IntStream randomInts = rand.ints(1000, 0, 100);

        int sum = randomInts.map(number -> number / 10).distinct().sum();
        System.out.println(sum);
    }

    public static void b() {
        int[] numbers = {2, 3, 5, 7};
        Random rand = new Random();

        IntSupplier randomChoice = () -> numbers[rand.nextInt(numbers.length)];

        int sum = IntStream.generate(randomChoice).limit(100).reduce(0, (n, m) -> n + m);
        System.out.println(sum);
    }

    public static void c() {
        Random rand = new Random();

        Supplier<Character> randomCharacter = () -> (char) (rand.nextInt(26) + 'a');
        Supplier<String> randomWord = () -> Stream.generate(randomCharacter).limit(rand.nextInt(5, 11)).map(c->c.toString()).collect(Collectors.joining(""));

        Stream<String> randomWords = Stream.generate(randomWord).limit(500);

        IntStream wordsLength = randomWords.mapToInt((s) -> s.length());

        System.out.println((wordsLength.sum() / 500.0));


    }
}
