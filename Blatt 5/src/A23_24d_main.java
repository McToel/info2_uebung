import java.util.Scanner;
import java.util.regex.Pattern;

public class A23_24d_main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        input.useDelimiter(Pattern.compile("[\\r\\n]+"));

        String name;
        int matrikelnummer;

        for (int i = 0; i < 3; i++) {
            try {
                System.out.print("Enter name: ");
                name = input.nextLine();

                System.out.print("Enter matrikelnummer: ");
                matrikelnummer = Integer.parseInt(input.nextLine());

                UniqueStudent myUniqueStudent = new UniqueStudent(name, matrikelnummer);
            } catch (DuplicateException e) {
                System.out.println("This matrikelnummer does already exist");
            } catch (Exception e) {
                break;
            }
        }

        System.out.print("Enter matrikelnummer to view details: ");
        matrikelnummer = Integer.parseInt(input.nextLine());

        if (UniqueStudent.students.containsKey(matrikelnummer)) {
            System.out.println(UniqueStudent.students.get(matrikelnummer));
        } else {
            System.out.println("Student does not exist");
        }
    }
}