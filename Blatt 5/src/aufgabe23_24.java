import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class aufgabe23_24 {
    public static void main(String[] args) {
        Student myStudent = new Student("Stephan Köttle", 1234567);
        System.out.println(myStudent);

        Scanner input = new Scanner(System.in);
        input.useDelimiter(Pattern.compile("[\\r\\n]+"));

        List<Student> myStudents = new ArrayList<>();
        myStudents.add(myStudent);

        String name;
        int matrikelnummer;

        try {
            while (true) {
                System.out.print("Enter name: ");
                name = input.nextLine();

                System.out.print("Enter matrikelnummer: ");
                matrikelnummer = Integer.parseInt(input.nextLine());

                myStudents.add(new Student(name, matrikelnummer));
            }
        } catch (Exception e) {
            System.out.println("Ungueltige Eingabe");
        }
        finally {
            System.out.println("Gespeicherte Students:");
            for (final Student student: myStudents
                 ) {
                System.out.println(student);
            }
        }
    }
}