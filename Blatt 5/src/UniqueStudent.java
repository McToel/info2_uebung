import java.util.Hashtable;

public class UniqueStudent extends Student {
    public static Hashtable<Integer, Student> students = new Hashtable<>();

    public UniqueStudent(String name, int matrikelnummer) throws DuplicateException {
        super(name, matrikelnummer);

        if (students.containsKey(this.getMatrikelnummer())) {
            throw new DuplicateException("matrikelnummer", Integer.toString(matrikelnummer));
        }
        else {
            students.put(this.getMatrikelnummer(), this);
        }
    }
}
