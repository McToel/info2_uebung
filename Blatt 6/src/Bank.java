import java.util.Hashtable;

public class Bank {
    private Hashtable<String, BankAccount> bankAccounts = new Hashtable<String, BankAccount>();

    public Bank(){

    }

    public void createBankAccount(String accountHolderName, String accountNumber) throws IllegalBankingException{
        if (bankAccounts.containsKey(accountNumber)) {
            throw new IllegalBankingException("Account number already taken");
        } else {
            bankAccounts.put(accountNumber, new BankAccount(accountHolderName, accountNumber));
        }
    }
    public double getBalanceByHolder(String accountHolderName) {
        return bankAccounts.values().stream()
                .filter(a -> a.getAccountHolderName().equals(accountHolderName))
                .mapToDouble(a -> a.getBalance())
                .sum();
    }

    public void addBalance(String accountNumber, double balance) throws IllegalBankingException {
        if (balance > 500) {
            throw new IllegalBankingException("Balance to high");
        } else if (balance < -500) {
            throw new IllegalBankingException("Balance to low");
        } else if (!bankAccounts.containsKey(accountNumber)) {
            throw new IllegalBankingException("Bank account does not exist");
        } else {
            if (balance > 0) {
                bankAccounts.get(accountNumber).deposit(balance);
            } else {
                bankAccounts.get(accountNumber).withdraw(-balance);
            }
        }

    }
}
