import javax.swing.*;
import java.awt.*;

public class A29b_main extends JFrame {

    public static void main(String[] args) {
        new A29b_main();
    }
    public A29b_main() {
        super("PIN");

        JTextField text = new JTextField();
        this.add(text, BorderLayout.NORTH);

        JPanel gridLayoutContainer = new JPanel();
        GridLayout numPad = new GridLayout(4, 3);

        gridLayoutContainer.setLayout(numPad);

        gridLayoutContainer.add(new JButton("7"));
        gridLayoutContainer.add(new JButton("8"));
        gridLayoutContainer.add(new JButton("8"));
        gridLayoutContainer.add(new JButton("4"));
        gridLayoutContainer.add(new JButton("5"));
        gridLayoutContainer.add(new JButton("6"));
        gridLayoutContainer.add(new JButton("1"));
        gridLayoutContainer.add(new JButton("2"));
        gridLayoutContainer.add(new JButton("3"));
        gridLayoutContainer.add(new JButton("0"));
        gridLayoutContainer.add(new JButton("Clear"));
        gridLayoutContainer.add(new JButton("Okay"));

        this.add(gridLayoutContainer, BorderLayout.SOUTH);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        pack();
        setVisible(true);
    }
}
