import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BankAccount {
    private String accountNumber;
    private String accountHolderName;
    private Date openingDate;
    private double balance;

    public BankAccount(String accountHolderName, String accountNumber) {
        setAccountNumber(accountNumber);
        setAccountHolderName(accountHolderName);
        setOpeningDate();
    }

    public boolean equals(BankAccount account) {
        return this.getAccountNumber().equals(account.getAccountNumber());
    }

    @Override
    public String toString() {
        return "(" + getAccountNumber() + ", " + getAccountHolderName() + ")";
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }
    public String getAccountHolderName() {
        return this.accountHolderName;
    }
    public Date getOpeningDate() {
        return this.openingDate;
    }

    public double getBalance() {
        return balance;
    }

    private boolean checkAccountNumber(String accountNumber) {
        String regex = "[A-Z0-9]{12}";
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(accountNumber);

        return matcher.matches() && matcher.start() == 0 && matcher.end() == accountNumber.length();
    }
    private boolean checkAccountHolderName(String accountHolderName) {
        String regex = "([A-Z]{1}[a-z]* )?([A-Z]{1}[a-z]*){1}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(accountHolderName);

        return matcher.matches() && matcher.start() == 0 && matcher.end() == accountHolderName.length();
    }

    private void setAccountNumber(String accountNumber) {
        if (checkAccountNumber(accountNumber)) {
            this.accountNumber = accountNumber;
        } else {
            throw new IllegalArgumentException("accountNumber invalid");
        }
    }

    public void setAccountHolderName(String accountHolderName) {
        if (checkAccountHolderName(accountHolderName)) {
            this.accountHolderName = accountHolderName;
        } else {
            throw new IllegalArgumentException("accountHolderName invalid");
        }
    }

    private void setOpeningDate() {
        this.openingDate = new Date();
    }

    public void withdraw(double withdrawAmount) {
        this.balance -= withdrawAmount;
    }

    public void deposit(double depositAmount) {
        this.balance += depositAmount;
    }


}

