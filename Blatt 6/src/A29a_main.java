import javax.swing.*;
import java.awt.*;

public class A29a_main extends JFrame {

    public static void main(String[] args) {
        new A29a_main();
    }
    public A29a_main() {
        super("Aufgabe29a");

        JPanel panelNorth = new JPanel();
        JTextField textField = new JTextField(20);
        panelNorth.add(new JLabel("Kontonummer"));
        panelNorth.add(textField);
        panelNorth.setBackground(new Color(200, 200, 200));

        JPanel panelSouth = new JPanel();
        JButton okayButton = new JButton("Okay");
        JButton cancelButton = new JButton("Abbrechen");
        panelSouth.add(okayButton);
        panelSouth.add(cancelButton);
        panelSouth.setBackground(new Color(150, 150, 150));

        this.add(panelNorth, BorderLayout.NORTH);
        this.add(panelSouth, BorderLayout.SOUTH);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        pack();
        setVisible(true);
    }
}
